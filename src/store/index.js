const merchantStore = {
    data: {},
    setData: function({
        value,
        key = String
    }) {
        this.data[key] = value
    }
}

const shopStore = {
    data: {},
    setData: function({
        value,
        key = String
    }) {
        this.data[key] = value
    }
}

module.exports = { 
    merchantStore,
    shopStore
}