const actions = {
    createMerchant: 'create-merchant',
    confirmMerchant: 'confirm-merchant',
    restartMerchant: 'restart-merchant',
    restartShop: 'restart-shop',
    confirmShop: 'confirm-shop',
    restartCashDrawer: 'restart-cash-drawer',
    confirmCashDrawer: 'confirm-cash-drawer',
    giveOwnQuestion: 'give-own-question'
}

const commands = {
    home: '/home',
    back: '/back',
    menu: '/menu',
    start: '/start',
    stop: '/stop',
    exit: '/exit'
}


module.exports = { actions, commands }