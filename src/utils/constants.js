const { lang } = require("../lang/lang")

const apiUrles = {
    allApplications: 'api/sales/support/lite/all-applications',
    updateApplication: 'api/sales/support/lite/update/application/',
    orders: 'orders',
    ordersAdd: 'orders/add',
    ordersUpdate: 'orders/update/',
    ordersStoreAdd: 'orders/store/add',
    branches: 'branches',
    branchesAdd: 'branches/add',
    branchesUpdate: 'branches/update/',
    properties: 'properties',
    propertiesAdd: 'properties/add',
    propertiesUpdate: 'properties/update/',
    propertiesDelete: 'properties/delete/', 
    offices: 'offices',
    officesAdd: 'offices/add',
    officesUpdate: 'offices/update/',
    storages: 'storages',
    storagesAdd: 'storages/add',
    storagesUpdate: 'storages/update/',
    signUp: 'api/administration/hr/add',
    signIn: 'api/auth/signin',
    getUsers: 'api/administration/hr',
    getRoles: 'all-roles',
    deleteUsers: 'api/administration/hr/delete/',
    updateUsers: 'api/administration/hr/update/',
    merchants: 'api/sales/support/lite/merchants',
    addMerchant: 'api/sales/support/lite/add-merchant-bot',
    upDateMerchant: 'api/sales/support/lite/update-merchant/',
    stores: 'api/sales/support/lite/all-stores/',
    addStore: 'api/sales/support/lite/add-store-bot',
    oneStore: 'api/sales/support/lite/one-store/',
    cashDrawersOfMerchant: 'api/sales/support/lite/all-terminals-bot/',
    cashDrawers: 'api/sales/support/lite/all-cash-drawers/',
    uploadContractFile: 'api/sales/support/lite/add-agreement-doc',
    addCashDrawer: 'api/sales/support/lite/add-terminal-bot',
    chinaHosting: 'http://ds.51zzd.com',
    // getUrban: '/control/api/mer/queryCity',
    checkBalanceWithChatId: 'api/sales/support/lite/check-status-tin-and-chat_id',
    checkBalance: 'info/subject/by-tin',
    infoRegions: 'info/regions',
    districtsByRegion: 'info/districts-by-region',
    activityGroup: 'info/activity-group',
    activityList: 'info/activity-list'
}

const scenes = {
    start: 'start',
    startEnterTin: 'start-enter-tin',
    merchant: 'merchant',
    merchantEnter: 'merchant-enter',
    merchantName: 'merchant-name',
    merchantPhone: 'merchant-phone',
    merchantPassword: 'merchant-password',
    merchantPasFile: 'merchant-passport-file',
    merchantConfirmation: 'merchant-confirmation',
    shop: {
        enterBack: 'enter-back',
        enterHome: 'enter-home',
        shopName: 'shop-name',
        region: 'region',
        district: 'district',
        activityGroup: 'activity-group',
        activityType: 'activity-type',
        cadastre: 'cadastre',
        address: 'address',
        location: 'location',
        cadastreFile: 'cadastre-file',
        confirmation: 'shop-confirmation'
    },
    cashDrawer: {
        enter: 'cash-drawer-enter',
        preBackEnter: 'cash-drawer-pre-back-enter',
        preHomeEnter: 'cash-drawer-pre-home-enter',
        model: 'model',
        typeOfApplication: 'type-of-application',
        typeOfApplicationSub: 'type-of-application-sub',
        contractFile: 'contract-file',
        applicationFile: 'application-file',
        confirmation: 'cash-drawer-confirmation'
    },
    merchantMenu: {
        home: 'home',
        noConnection: 'merchant-menu-no-connection',
        noMoney: 'merchant-menu-no-money',
        revice: 'merchant-menu-revice',
        listShops: 'list-shops',
        listCashDrawers: 'list-cash-drawers',
        addShop: 'add-shop-merchant-menu',
        cashDrawerUploadFile: 'cash-drawer-upload-file',
        // faq: 'faq',
        exit: 'menu-exit'
    },
    faq: {
        preBack: 'faq-pre-back',
        enter: 'faq-enter',
        shop: 'shop',
        cashDrawer: 'cash-drawer',
        revice: 'revice',
        giveQuestion: 'give-question'
    }
}

const typeConnection = [
    {
        id: '1',
        title: lang.dialog.cashDrawer.buy
    },
    {
        id: '2',
        title: lang.dialog.cashDrawer.connection
    }
]

const typeConnectionSub = [
    {
        id: '3',
        title: lang.dialog.cashDrawer.bank
    },
    {
        id: '4',
        title: lang.dialog.cashDrawer.other
    }
]

const modelNoData = [
    {
        title: 'PAX A-930',
        id: '3'
    }
]


module.exports = { 
    apiUrles,
    scenes,
    modelNoData,
    typeConnection,
    typeConnectionSub
}