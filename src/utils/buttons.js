const { Markup, Telegraf } = require("telegraf");

const nextButton = Markup.inlineKeyboard([
    [Markup.button.callback('ttest', 'test')]
])

// const testMenu = Telegraf.Extra
//   .markdown()
//   .markup((m) => m.inlineKeyboard([
//     m.callbackButton('Test button', 'test')
//   ]))

module.exports = { nextButton }