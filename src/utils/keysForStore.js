const merchantKeys = {
    chatId: 'chatId',
    nickName: 'nickName',
    userMobile: 'userMobile',
    userPasswd: 'userPasswd',
    passFile: 'passFile',
    merchantId: 'merchantId',
    tin: 'tin',
    merMapId: 'merMapId'
}

const shopKeys = {
    shopName: 'shopName',
    merMapId: 'merMapId',
    cityAddress: 'cityAddress',
    regionCode: 'regionCode',
    regionName: 'regionName',
    districtCode: 'districtCode',
    districtName: 'districtName',
    activityGroup: 'activityGroup',
    activityGroupName: 'activityGroupName',
    activityType: 'activityType',
    activityTypeName: 'activityTypeName',
    cadastreNumber: 'cadastreNumber',
    cadastreFile: 'fileName',
    latitude: 'latitude',
    longitude: 'longitude',
    tin: 'tin',
    merchantId: 'merchantId',
    shopId: 'shopId',
    shopMapId: 'shopMapId'
}

const cashDrawerKeys = {
    ccmCategoryId: 'ccmCategoryId',
    ccmName: 'ccmName',
    shopId: 'shopId',
    shopMapId: 'shopMapId',
    merchantId: 'merchantId',
    ccmSerialNumber: 'ccmSerialNumber',
    fiskalMod: 'fiskalMod',
    officeId: 'officeId',
    phone: 'phone',
    contractNumber: 'contractNumber',
    date: 'date',
    typeOfApplication: 'typeOfApplication',
    typeOfApplicationName: 'typeOfApplicationName',
    contractFile: 'contractFile',
    applicationFile: 'applicationFile',
    tin: 'tin',
}

module.exports = { 
    merchantKeys,
    shopKeys,
    cashDrawerKeys
}