
/**
 * Wrapper to catch async errors within a stage. Helps to avoid try catch blocks in there
 * @param fn - function to enter a stage
 */
const asyncWrapper = fn => {
  return async function(ctx, next) {
    try {
      return await fn(ctx)
    } catch (error) {
      console.log('asyncWrapper error, %O', error)
      await ctx.reply('shared.something_went_wrong')
      return next()
    }
  }
}

module.exports = { asyncWrapper }