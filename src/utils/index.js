import { Markup } from "telegraf"
import { lang } from "../lang/lang"

export const getBackKeyboard = ctx => {
    // const backKeyboardBack = ctx.i18n.t('keyboards.back_keyboard.back')
    let backKeyboard = Markup.keyboard([lang.keyboards.back])

    backKeyboard = backKeyboard.resize().extra()

    return {
        backKeyboard,
        // backKeyboardBack
    }
}