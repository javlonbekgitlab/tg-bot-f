const wordsRu = {
    keyboards: {
        back: 'Назад',
        next: 'Далее',
        add: 'Добавить'
    },
    dialog: {
        texts: {
            hello: 'Привет',
            infoEnter: 'Информационная часть*'
        },
        merchant: {
            balance: 'На вашем счету: ',
            currency: 'сум',
            orgName: 'Наименование организации: ',
            addressOrg: 'Адрес: ',
            enterTin: 'Введите ИНН',
            name: 'Введите ваше Имя и Фамилию:',
            phone: 'Введите ваш телефонный номер:',
            pwd: 'Придумайте 6 значный цифровой пароль:',
            pasFile: 'Загрузите скан паспорта в формате PDF',
            confirm: {
                fullName: 'Имя и Фамилия: ',
                phone: 'Телефонный номер: ',
                password: 'Пароль: '
            },
        },
        shop: {
            addShop: 'Добавить Магазин',
            shopName: 'Название предприятия:',
            region: 'Укажите регион:',
            district: 'Укажите район/город:',
            activityGroup: 'Укажите группу видов деятельности:',
            activityType: 'Укажите вид деятельности:',
            cadastreNumber: 'Укажите номер кадастра/аренды:',
            address: 'Адрес:',
            location: 'Укажите вашу геолокацию:',
            cadastreFile: 'Загрузите файл договора аренды/кадастра в формате (pdf|docx|doc):',
            confirm: {
                shopName: 'Название предприятия: ',
                region: 'Регион: ',
                district: 'Район/город: ',
                activityGroup: 'Группа видов деятельности: ',
                activityType: 'Вид деятельности: ',
                cadastreNumber: 'Номер кадастра/аренды: ',
                address: 'Адрес:',
            }
        },
        cashDrawer: {
            model: 'Выберите модель: ',
            modelTitle: 'Модель: ',
            fiskalMod: 'Фискальный модуль: ',
            serialNumber: 'Серийный номер: ',
            typeOfApplication: 'Тип заявления: ',
            contractFile: 'Файл договора - объяснение*',
            applicationFile: 'Заявление - объяснение*',
            buy: 'Покупка',
            connection: 'Подключение',
            bank: 'Банк',
            other: 'Прочее'
        },
        merchantMenu: {
            welcome: 'Добро пожаловать',
            chooseSection: 'Выберите раздел меню:',
            menu: 'Меню',
            revice: 'АКТ СВЕРКИ',
            listShops: 'СПИСОК МАГАЗИНОВ/\nДОБАВЛЕНИЕ ТЕРМИНАЛОВ',
            listCashDrawers: 'СПИСОК ТЕРМИНАЛОВ',
            addContractFile: 'ДОБАВИТЬ ДОГОВОР',
            messageUploadedFile: 'Ваше заявление отправлено на подтверждение, ожидаейте уведомление в течении 24 часов',
            uploadContractFile: 'Отправьте файл договора для завершения формировки заявления',
            addShop: 'ДОБАВЛЕНИЕ МАГАЗИНА',
            faq: 'FAQ',
            exit: 'ВЫХОД',
            hello: 'Здравствуйте',
            addCashDrawer: 'ДОБАВИТЬ ТЕРМИНАЛ'
        },
        faq: {
            primaryTitle: 'Вы можете найти свою проблему изучив рабочие разделы бота ARCA GROUP',
            title: 'Вот список часто задаваемых вопросов:',
            shop: 'КАК СОЗДАТЬ МАГАЗИН',
            cashDrawer: 'КАК ДОБАВИТЬ ТЕРМИНАЛ',
            revice: 'ЧТО ТАКОЕ АКТ СВЕРКИ',
            giveQuestion: 'ЗАДАТЬ СВОЙ ВОПРОС',
            titleExtraQuestion: 'Если возникли ещё вопросы, задайте их напрямую оператору.'
        }
    }
}

module.exports = { wordsRu }