const axios = require('axios')
require('dotenv').config()

const Api = async({host = process.env.SERVER_HOST, url, data = null, type = 'get'}) => {
    let response = {}
    const config = {
        headers: {
            'Content-Type': 'application/json',
            'x-access-token': process.env.SERVER_TOKEN
        },
    }
    // config.headers['x-access-token'] = ``)
    if (type === 'get') {
        try {
            response = await axios.get(`${host}${url}`, config)
        } catch (error) {
            response = error
        }
    }
    if (type === 'post') {
        try {
            response = await axios.post(`${host}${url}`, data, config)
        } catch (error) {
            console.log('eeeeeeeeeeeeeeeeeeeeeerrrrrrrrrrrrrrrrrrrrr',error)
            response = { ...error, status: 500}
        }
    }
    return response
}

module.exports = { Api }
