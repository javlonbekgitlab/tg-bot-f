const { onCheckBalanceByTin } = require("./getDataFromServer")

const autoSelectName = ({
    data, 
    keyOfValue, 
    keyOfData, 
    value
}) => {
    const selected = data.find(obj => {
        return obj[keyOfData] === value
    })
    return selected[keyOfValue]
}

const checkBalance = async tin => {
    const { success, data } = await onCheckBalanceByTin(tin)
    console.log(data)
    if (success && data && data.terminalCost) {
        const {
            terminalCost,
            overpaymentSum
        } = data
        const debt = overpaymentSum - terminalCost
        if (debt < 0) {
            // debt
            return -1
        } 
        if (debt >= 0) {
            // ok
            return 1
        }
    } else {
        // problem with connection
        return 0
    }
}

module.exports = { 
    autoSelectName,
    checkBalance
}