Array.prototype.customLength = function(){
    let condition = true
    let counter = 0
    while(condition) {
        if (this[counter]) {
            counter ++
        } else {
            condition = false
        }
    }   
    return counter
}

const lengthOfArray = [1,2,6,5].customLength()
console.log(lengthOfArray)