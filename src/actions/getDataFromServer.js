const { Api } = require("../api")
const { apiUrles } = require("../utils/constants")

const onCheckBalanceByTin = async tin => {
    const response = {}
    const res = await Api({
        url: `${apiUrles.checkBalance}?tin=${tin}`
    })
    if (res.status === 200) {
        response.success = true
        response.data = res.data
    } else {
        response.success = false
    }
    return response
}

module.exports = { onCheckBalanceByTin }