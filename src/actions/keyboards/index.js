// const { botCommands } = require("../../src/lang/constants");
// const { lang } = require("../../src/lang");

const { lang } = require("../../lang/lang")
const { commands } = require("../../utils/callback-commands")

// const selectPart = [
//     [
//         {
//             text: lang.keyboards.accounting,
//             callback_data: botCommands.accounting
//         },
//         {
//             text: lang.keyboards.signUpMerchant,
//             callback_data: botCommands.signUpNits
//         }
//     ]

// ]
const buttonNext = [[{
    text: lang.keyboards.next,
    callback_data: commands.next
}]]

// const universalNextButton = callback_data => [[{
//     text: lang.keyboards.next,
//     callback_data
// }]]

module.exports = {
    // selectPart,
    buttonNext
}