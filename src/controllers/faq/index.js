const { Scenes, Markup } = require("telegraf");
const { lang } = require("../../lang/lang");
const { actions, commands } = require("../../utils/actions");
const { scenes } = require("../../utils/constants");
require('../../actions/deleteHistory.js')

const {
    primaryTitle,
    title,
    shop,
    cashDrawer,
    revice,
    giveQuestion,
    titleExtraQuestion
} = lang.dialog.faq

const faqPreBackScene = new Scenes.BaseScene(scenes.faq.preBack)
faqPreBackScene.enter(async ctx => {
    const { message_id } = await ctx.reply(primaryTitle, Markup
        .keyboard([
            [commands.back]
        ]).resize().oneTime(true)
    )
    ctx.session.idOfCommandButtonFaq = [message_id]
    ctx.session.nextScene = scenes.cashDrawer.preBackEnter
    ctx.scene.enter(scenes.faq.enter)
})

const faqEnterScene = new Scenes.BaseScene(scenes.faq.enter)
faqEnterScene.enter(async ctx => {
    const { message_id } = await ctx.reply(title, {
        reply_markup: {
            inline_keyboard: [
                [{text: shop, callback_data: scenes.faq.shop}],
                [{text: cashDrawer, callback_data: scenes.faq.cashDrawer}],
                [{text: revice, callback_data: scenes.faq.revice}],
                [{text: giveQuestion, callback_data: scenes.faq.giveQuestion}]
            ]
        }
    })
    ctx.session.idsOfmessagesFaqScene = [message_id]
    faqEnterScene.command([commands.back], async ctx => {
        const { idOfCommandButtonFaq } = ctx.session
        await ctx.deleteMessage()
        await idOfCommandButtonFaq.deleteMessages(ctx)
        ctx.scene.enter(scenes.merchantMenu.home)
    })

    faqEnterScene.on('callback_query', ctx => {
        ctx.scene.enter(ctx.update.callback_query.data)
    })

    faqEnterScene.leave( async ctx => {
        let { idsOfmessagesFaqScene } = ctx.session
        idsOfmessagesFaqScene.deleteMessages(ctx)
        ctx.session.idsOfmessagesFaqScene = []
    })
})

const faqShopScene = new Scenes.BaseScene(scenes.faq.shop)
faqShopScene.enter( async ctx => {
    const { message_id } = await ctx.reply('how add a shop')
    ctx.session.idsOfmessagesFaqScene.push(message_id)
    ctx.scene.enter(scenes.faq.giveQuestion)
})

const faqCashDrawerScene = new Scenes.BaseScene(scenes.faq.cashDrawer)
faqCashDrawerScene.enter( async ctx => {
    const { message_id } = await ctx.reply('how add a cash')
    ctx.session.idsOfmessagesFaqScene.push(message_id)
    ctx.scene.enter(scenes.faq.giveQuestion)
})

const faqReviceScene = new Scenes.BaseScene(scenes.faq.revice)
faqReviceScene.enter( async ctx => {
    const { message_id } = await ctx.reply('what is revice answer')
    ctx.session.idsOfmessagesFaqScene.push(message_id)
    ctx.scene.enter(scenes.faq.giveQuestion)
})

const faqGiveQuestionScene = new Scenes.BaseScene(scenes.faq.giveQuestion)
faqGiveQuestionScene.enter(async ctx => {
    const repliedMessage = await ctx.reply(titleExtraQuestion)
    const repliedContact = await ctx.replyWithContact('+998900323666', '@ratregem')
    ctx.session.idsOfmessagesFaqScene.push(repliedMessage.message_id)
    ctx.session.idsOfmessagesFaqScene.push(repliedContact.message_id)
    faqGiveQuestionScene.command([commands.back], async ctx => {
        await ctx.deleteMessage()
        ctx.scene.enter(scenes.faq.enter)
    })
    faqGiveQuestionScene.leave( async ctx => {
        let { idsOfmessagesFaqScene } = ctx.session
        idsOfmessagesFaqScene.deleteMessages(ctx)
        ctx.session.idsOfmessagesFaqScene = []
    })
})



module.exports = {
    faqPreBackScene,
    faqEnterScene,
    faqShopScene,
    faqCashDrawerScene,
    faqReviceScene,
    faqGiveQuestionScene
}