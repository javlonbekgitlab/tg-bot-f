const { Scenes, Markup } = require("telegraf")
const { buttonNext } = require("../../actions/keyboards")
const { lang } = require("../../lang/lang")
const { merchantStore } = require("../../store")
const { commands } = require("../../utils/actions")
// const { merchantStore } = require("../../store")
const { testMenu } = require("../../utils/buttons")
const { scenes } = require("../../utils/constants")
const { asyncWrapper } = require("../../utils/error-handlers")
const { merchantKeys } = require("../../utils/keysForStore")
require('../../actions/deleteHistory.js')
// const { nextButton } = require("../../utils/buttons")

const {
    hello,
    infoEnter
} = lang.dialog.texts

const startScene = new Scenes.BaseScene(scenes.start)

startScene.enter(async ctx => {
    await ctx.reply(infoEnter)
    await ctx.reply(`${hello} ${ctx.update.message.chat.first_name}`, Markup
        .keyboard([
            [commands.stop]
        ]).resize().oneTime(true)
    )
    ctx.scene.enter(scenes.startEnterTin)
})

const startEnterTinScene = new Scenes.BaseScene(scenes.startEnterTin)
startEnterTinScene.enter( async ctx => {
    ctx.session.merchantStore = {
        data: {},
        setData: function({
            value,
            key = String
        }) {
            this.data[key] = value
        }
    }
    await ctx.reply(lang.dialog.merchant.enterTin)
    // ctx.session.idOfMessageInfoStart = [repliedMessageInfo.message_id]
    // ctx.session.idsOfMessagesStart = [repliedMessage.message_id, repliedMessageTin.message_id]
    startEnterTinScene.command([commands.stop], async ctx => {
        // const { idOfMessageInfoStart } = ctx.session
        // await idOfMessageInfoStart.deleteMessages(ctx)
        // ctx.session.idOfMessageInfoStart = []
        ctx.scene.leave()
        ctx.reply('exit', Markup
            .keyboard([
                [commands.start]
            ]).resize().oneTime(true)
        )
    })
    startEnterTinScene.on('text', ctx => {
        // ctx.session.idsOfMessagesStart.push(ctx.update.message.message_id)
        ctx.session.merchantStore.setData({
            key: merchantKeys.tin,
            value: ctx.update.message.text
        })
        ctx.scene.enter(scenes.merchant)
    })
    startEnterTinScene.on('message', ctx => ctx.deleteMessage())
    // startScene.leave( async ctx => {
    //     let { idsOfMessagesStart } = ctx.session
    //     idsOfMessagesStart.deleteMessages(ctx)
    //     ctx.session.idsOfMessagesStart = []
    // })
})

module.exports = {
    startScene,
    startEnterTinScene
}