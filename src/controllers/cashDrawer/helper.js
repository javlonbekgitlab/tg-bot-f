const { Api } = require("../../api")
const { apiUrles } = require("../../utils/constants")

const onSubmitCashDrawer = async data => {
    const response = {}
    const res = await Api({
        data,
        type: 'post',
        url: apiUrles.addCashDrawer
    })
    if (res.status === 200) {
        response.success = true
        response.data = res.data
    } else {
        response.success = false
    }
    // progressForm.setProgressForm(0)
    return response
}

module.exports = {
    onSubmitCashDrawer
}