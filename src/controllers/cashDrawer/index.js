const { Scenes, Markup } = require("telegraf");
const { autoSelectName } = require("../../actions");
const { lang } = require("../../lang/lang");
const { scenes, modelNoData, typeConnection, typeConnectionSub } = require("../../utils/constants");
const { cashDrawerKeys } = require("../../utils/keysForStore");
const path = require('path');
const { onSubmitCashDrawer } = require("./helper");
const { commands, actions } = require("../../utils/actions");
require('../../actions/deleteHistory.js')

const {
    model,
    modelTitle,
    typeOfApplication,
    contractFile,
    applicationFile
} = lang.dialog.cashDrawer

const cashDrawerPreHomeEnterScene = new Scenes.BaseScene(scenes.cashDrawer.preHomeEnter)
cashDrawerPreHomeEnterScene.enter( async ctx => {
    const { message_id } = await ctx.reply(lang.dialog.merchantMenu.addCashDrawer, Markup
        .keyboard([
            [commands.stop]
        ]).resize().oneTime(true)
    )
    ctx.session.nextSceneForCashDrawer = scenes.start 
    ctx.session.idsOfCommandButtonCashDrawer = [message_id]
    ctx.scene.enter(scenes.cashDrawer.model)
})

const cashDrawerPreBackEnterScene = new Scenes.BaseScene(scenes.cashDrawer.preBackEnter)
cashDrawerPreBackEnterScene.enter( async ctx => {
    const { message_id } = await ctx.reply(lang.dialog.merchantMenu.addCashDrawer, Markup
        .keyboard([
            [commands.back]
        ]).resize().oneTime(true)
    )
    ctx.session.nextSceneForCashDrawer = scenes.merchantMenu.home 
    ctx.session.idsOfCommandButtonCashDrawer = [message_id]
    ctx.scene.enter(scenes.cashDrawer.model)
})

// const cashDrawerEnterScene = new Scenes.BaseScene(scenes.cashDrawer.enter)
// cashDrawerEnterScene.enter( async ctx => {
//     if (!ctx.session.cashDrawerStore) {
//         ctx.session.cashDrawerStore = {
//             data: {},
//             setData: function({
//                 value,
//                 key = String
//             }) {
//                 this.data[key] = value
//             }
//         }
//     }
//     ctx.scene.enter(scenes.cashDrawer.model)
// })

const cashDrawerModelScene = new Scenes.BaseScene(scenes.cashDrawer.model)
cashDrawerModelScene.enter( async ctx => {
    ctx.session.models = modelNoData.map(item => [{text: item.title, callback_data: item.id}])
    const { message_id } = await ctx.reply(model, {
        reply_markup: {
            inline_keyboard: ctx.session.models,
        },
    })
    ctx.session.idsOfmessagesCashDrawerScene = [message_id]
    cashDrawerModelScene.command([commands.back], async ctx => {
        await ctx.deleteMessage()
        const { idsOfCommandButtonCashDrawer } = ctx.session
        await idsOfCommandButtonCashDrawer.deleteMessages(ctx)
        ctx.scene.enter(scenes.merchantMenu.listShops)
    })
    cashDrawerModelScene.command([commands.stop], async ctx => {
        await ctx.deleteMessage()
        const { idsOfCommandButtonCashDrawer } = ctx.session
        await idsOfCommandButtonCashDrawer.deleteMessages(ctx)
        ctx.scene.leave()
        ctx.reply('bayy', Markup
            .keyboard([
                [commands.start]
            ]).resize().oneTime(true)
        )
    })
    cashDrawerModelScene.on('callback_query', ctx => {
        ctx.session.cashDrawerStore.setData({
            key: cashDrawerKeys.ccmCategoryId,
            value: ctx.update.callback_query.data
        })
        ctx.session.cashDrawerStore.setData({
            key: cashDrawerKeys.ccmName,
            value: autoSelectName({
                data: modelNoData,
                keyOfData: 'id',
                keyOfValue: 'title',
                value: ctx.update.callback_query.data
            })
        })
        // console.log(ctx.session.cashDrawerStore.data)
        // ctx.scene.enter(scenes.cashDrawer.typeOfApplication)
        ctx.scene.enter(scenes.cashDrawer.typeOfApplication)
    })
    cashDrawerModelScene.on('message', ctx => ctx.deleteMessage())
    cashDrawerModelScene.leave( async ctx => {
        const { idsOfmessagesCashDrawerScene } = ctx.session
        await idsOfmessagesCashDrawerScene.deleteMessages(ctx)
        ctx.session.idsOfmessagesCashDrawerScene = []
    })
})

const cashDrawerTypeOfApplicationScene = new Scenes.BaseScene(scenes.cashDrawer.typeOfApplication)
cashDrawerTypeOfApplicationScene.enter( async ctx => {
    const connectionTypes = typeConnection.map(item => {
        return [{text: item.title, callback_data: item.id}]
    })
    const { message_id } = await ctx.reply(typeOfApplication, {
        reply_markup: {
            inline_keyboard: connectionTypes,
        },
    })
    ctx.session.idsOfmessagesCashDrawerScene.push(message_id)
    cashDrawerTypeOfApplicationScene.command([commands.back], async ctx => {
        await ctx.deleteMessage()
        const { idsOfCommandButtonCashDrawer } = ctx.session
        await idsOfCommandButtonCashDrawer.deleteMessages(ctx)
        ctx.scene.enter(scenes.merchantMenu.listShops)
    })
    cashDrawerTypeOfApplicationScene.command([commands.stop], async ctx => {
        await ctx.deleteMessage()
        const { idsOfCommandButtonCashDrawer } = ctx.session
        await idsOfCommandButtonCashDrawer.deleteMessages(ctx)
        ctx.scene.leave()
        ctx.reply('bayy', Markup
            .keyboard([
                [commands.start]
            ]).resize().oneTime(true)
        )
    })
    cashDrawerTypeOfApplicationScene.on('callback_query', ctx => {
        // ctx.deleteMessage()
        console.log('type connection', ctx.update.callback_query.data)
        if (ctx.update.callback_query.data === '1') {
            ctx.session.cashDrawerStore.setData({
                key: cashDrawerKeys.typeOfApplication,
                value: ctx.update.callback_query.data
            })
            ctx.session.cashDrawerStore.setData({
                key: cashDrawerKeys.typeOfApplicationName,
                value: autoSelectName({
                    data: typeConnection,
                    keyOfData: 'id',
                    keyOfValue: 'title',
                    value: ctx.update.callback_query.data
                })
            })
            ctx.scene.enter(scenes.cashDrawer.confirmation)
        } else {
            ctx.scene.enter(scenes.cashDrawer.typeOfApplicationSub)
        }
    })
    cashDrawerTypeOfApplicationScene.on('message', ctx => ctx.deleteMessage())
    cashDrawerTypeOfApplicationScene.leave( async ctx => {
        const { idsOfmessagesCashDrawerScene } = ctx.session
        await idsOfmessagesCashDrawerScene.deleteMessages(ctx)
        ctx.session.idsOfmessagesCashDrawerScene = []
    })
})

const cashDrawerTypeOfApplicationSubScene = new Scenes.BaseScene(scenes.cashDrawer.typeOfApplicationSub)
cashDrawerTypeOfApplicationSubScene.enter( async ctx => {
    const connectionTypes = typeConnectionSub.map(item => {
        return [{text: item.title, callback_data: item.id}]
    })
    const { message_id } = await ctx.reply(typeOfApplication, {
        reply_markup: {
            inline_keyboard: connectionTypes,
        },
    })
    ctx.session.idsOfmessagesCashDrawerScene.push(message_id)
    cashDrawerTypeOfApplicationSubScene.command([commands.back], async ctx => {
        await ctx.deleteMessage()
        const { idsOfCommandButtonCashDrawer } = ctx.session
        await idsOfCommandButtonCashDrawer.deleteMessages(ctx)
        ctx.scene.enter(scenes.merchantMenu.listShops)
    })
    cashDrawerTypeOfApplicationSubScene.command([commands.stop], async ctx => {
        await ctx.deleteMessage()
        const { idsOfCommandButtonCashDrawer } = ctx.session
        await idsOfCommandButtonCashDrawer.deleteMessages(ctx)
        ctx.scene.leave()
        ctx.reply('bayy', Markup
            .keyboard([
                [commands.start]
            ]).resize().oneTime(true)
        )
    })
    cashDrawerTypeOfApplicationSubScene.on('callback_query', ctx => {
        // ctx.deleteMessage()
        console.log('type connection', ctx.update.callback_query.data)
        ctx.session.cashDrawerStore.setData({
            key: cashDrawerKeys.typeOfApplication,
            value: ctx.update.callback_query.data
        })
        ctx.session.cashDrawerStore.setData({
            key: cashDrawerKeys.typeOfApplicationName,
            value: lang.dialog.cashDrawer.connection
        })
        console.log(ctx.session.cashDrawerStore.data)
        // ctx.scene.enter(scenes.cashDrawer.contractFile)////////////
        ctx.scene.enter(scenes.cashDrawer.confirmation)////////////
    })
    cashDrawerTypeOfApplicationSubScene.on('message', ctx => ctx.deleteMessage())
    cashDrawerTypeOfApplicationSubScene.leave( async ctx => {
        const { idsOfmessagesCashDrawerScene } = ctx.session
        await idsOfmessagesCashDrawerScene.deleteMessages(ctx)
        ctx.session.idsOfmessagesCashDrawerScene = []
    })
})


const cashDrawerConfirmationScene = new Scenes.BaseScene(scenes.cashDrawer.confirmation)
cashDrawerConfirmationScene.enter(async ctx => {
    console.log(ctx.session.cashDrawerStore.data)
    const { ccmName, typeOfApplicationName } = ctx.session.cashDrawerStore.data
    const { message_id } = await ctx.reply(
        `${modelTitle}${ccmName}\n` + 
        `${typeOfApplication}${typeOfApplicationName}`, {
            reply_markup: {
                inline_keyboard: [
                    [{text:lang.keyboards.back, callback_data: actions.restartCashDrawer},
                    {text:lang.keyboards.add, callback_data: actions.confirmCashDrawer}]
                ]
            }
    })
    ctx.session.idOfCashDrawerConfirmation = message_id
    ctx.session.idsOfmessagesCashDrawerScene.push(message_id)
    cashDrawerConfirmationScene.command([commands.back], async ctx => {
        await ctx.deleteMessage()
        // const { idsOfCommandButtonCashDrawer, idsOfmessagesCashDrawerScene } = ctx.session
        // await idsOfCommandButtonCashDrawer.deleteMessages(ctx)
        // await idsOfmessagesCashDrawerScene.deleteMessages(ctx)
        // delete ctx.session.idOfCashDrawerConfirmation
        // ctx.session.idsOfmessagesCashDrawerScene = []
        ctx.scene.enter(scenes.merchantMenu.listShops)
    })
    cashDrawerConfirmationScene.command([commands.stop], async ctx => {
        await ctx.deleteMessage()
        ctx.scene.leave()
        ctx.reply('bayy', Markup
            .keyboard([
                [commands.start]
            ]).resize().oneTime(true)
        )
    })
    cashDrawerConfirmationScene.action(actions.restartCashDrawer, async ctx => {
        console.log(ctx.session.cashDrawerStore.data)
        const { idsOfmessagesCashDrawerScene } = ctx.session
        await idsOfmessagesCashDrawerScene.deleteMessages(ctx)
        ctx.session.idsOfmessagesCashDrawerScene = []
        delete ctx.session.idOfCashDrawerConfirmation
        ctx.scene.enter(scenes.cashDrawer.model)
    })
    cashDrawerConfirmationScene.action(actions.confirmCashDrawer, async ctx => {
        const { success, data } = await onSubmitCashDrawer(ctx.session.cashDrawerStore.data)
        if (success && data) {
            console.log('datttaafromserverere', data)
            if (data.error || data.message) {
                await ctx.reply(data.message)
                ctx.scene.enter(scenes.merchantMenu.home)
            } else {
                await ctx.deleteMessage(ctx.session.idOfCashDrawerConfirmation)
                ctx.session.idsOfmessagesCashDrawerScene = []
                delete ctx.session.idOfCashDrawerConfirmation
                await ctx.reply(applicationFile)
                // await ctx.replyWithDocument({url: "http://10.0.2.202:8005/api/sales/support/lite/all-stores/upload/files/1624542591047_dog.pdf"})
                try {
                    data.applicationFile.name && data.applicationFile.source && await ctx.replyWithDocument({
                        url: data.applicationFile.source, 
                        filename: data.applicationFile.name
                    })
                } catch (error) {
                    console.log(error)
                    ctx.reply('problem with back')
                }
                // const { idsOfCommandButtonCashDrawer } = ctx.session
                // await idsOfCommandButtonCashDrawer.deleteMessages(ctx)
                ctx.scene.enter(scenes.cashDrawer.contractFile)
            }
            // ctx.scene.enter(ctx.session.nextSceneForCashDrawer)
        } else {
            await ctx.reply('problem ')
            console.log(data)
        }
    })
    cashDrawerConfirmationScene.on('message', ctx => ctx.deleteMessage())
    cashDrawerConfirmationScene.leave( async ctx => {
        const { idsOfCommandButtonCashDrawer, idsOfmessagesCashDrawerScene } = ctx.session
        await idsOfCommandButtonCashDrawer.deleteMessages(ctx)
        await idsOfmessagesCashDrawerScene.deleteMessages(ctx)
        delete ctx.session.idOfCashDrawerConfirmation
        ctx.session.idsOfmessagesCashDrawerScene = []
    })
        // delete ctx.session.cashDrawerStore
        // ctx.scene.enter(scenes.shop.shopName)
})
// cashDrawerConfirmationScene.leave(ctx => {
// })


const cashDrawerContractFileScene = new Scenes.BaseScene(scenes.cashDrawer.contractFile)
cashDrawerContractFileScene.enter(async ctx => {
    // const fileUrl = 'http://10.0.2.202:8005/api/sales/support/lite/all-stores/upload/files/1624617347134_dog.pdf'
    // await ctx.replyWithDocument({
        //     url: fileUrl, 
        //     filename: newFileName[newFileName.length - 1]
        // })
    const contractFileSource = path.join(__dirname, '../../assets/contractfile/testCont.pdf')
    const newFileName = contractFileSource.split('/')
    const repliedMessage = await ctx.reply(contractFile)
    // ctx.session.idsOfmessagesCashDrawerScene.push(repliedMessage.message_id)
    const repliedDocument = await ctx.replyWithDocument({
        source: contractFileSource, 
        filename: newFileName[newFileName.length - 1]
    })
    // ctx.session.idsOfmessagesCashDrawerScene.push(repliedDocument.message_id)
    // ctx.scene.enter(scenes.cashDrawer.confirmation)
    ctx.scene.enter(ctx.session.nextSceneForCashDrawer)
    delete ctx.session.nextSceneForCashDrawer
    ctx.session.cashDrawerStore.data = {}
    ctx.session.idsOfmessagesCashDrawerScene = []
})


module.exports = {
    cashDrawerPreHomeEnterScene,
    cashDrawerPreBackEnterScene,
    // cashDrawerEnterScene,
    cashDrawerModelScene,
    cashDrawerTypeOfApplicationScene,
    cashDrawerTypeOfApplicationSubScene,
    cashDrawerContractFileScene,
    cashDrawerConfirmationScene
}
