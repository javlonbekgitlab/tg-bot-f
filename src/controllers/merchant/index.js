const { Scenes, Markup } = require("telegraf")
const { lang } = require("../../lang/lang")
// const { merchantStore } = require("../../store")
const { actions, commands } = require("../../utils/actions")
const { scenes } = require("../../utils/constants")
const { merchantKeys, shopKeys } = require("../../utils/keysForStore")
const { onGetInfoByTin, onSubmitMerchant } = require("./helper")
const fs = require('fs')
require('../../actions/deleteHistory.js')


const {
    balance,
    currency,
    orgName,
    addressOrg,
    name,
    phone,
    pwd,
    pasFile,
    confirm
} = lang.dialog.merchant

// const merchantEnterScene = new Scenes.BaseScene(scenes.merchantEnter)
// merchantEnterScene.enter(async ctx => {
//     ctx.reply('create-merchant', Markup
//         .keyboard([
//             [commands.exit//         ]).resize().oneTime(true)
//     )
//     ctx.scene.enter(scenes.merchant)
// })
// merchantEnterScene.leave(ctx => ctx.deleteMessage())


const merchantScene = new Scenes.BaseScene(scenes.merchant)
merchantScene.enter(async ctx => {
    const { merchantStore } = ctx.session
    const { success, data } = await onGetInfoByTin({
        tinOrPinfl: merchantStore.data.tin,
        chatId: ctx.update.message.chat.id
    })
    console.log('status merchant',data, success)
    ctx.session.idsOfMessagesMerchant = []
    // console.log(success , data.address , data.regionCode , data.districtCode)
    if (success && data && data._id && data.tin && data.merMapId) {
        const { _id, tin, merMapId } = data
        ctx.session.merchantStore.setData({
            key: merchantKeys.merchantId,
            value: _id
        })
        ctx.session.merchantStore.setData({
            key: merchantKeys.tin,
            value: tin
        })
        ctx.session.merchantStore.setData({
            key: merchantKeys.merMapId,
            value: merMapId
        })
        console.log(ctx.session.merchantStore)
        ctx.scene.enter(scenes.merchantMenu.home)
    } else if (!success) {
        const { message_id } = await ctx.reply('no connection')
        // ctx.session.idsOfMessagesMerchant.push(message_id)
        ctx.scene.enter(scenes.startEnterTin)
    } else if (success && !data || (Object.keys(data).length < 1)) {
        const { message_id } = await ctx.reply(`not found`)
        // ctx.session.idsOfMessagesMerchant.push(message_id)
        ctx.scene.enter(scenes.startEnterTin)
    } else {
        if (success && data && data.address && data.regionCode && data.districtCode) {
            const {
                address = '', 
                // regionName = '', 
                districtName = '', 
                name,
                terminalCost = undefined, 
                overpaymentSum = undefined
            } = data
            console.log(terminalCost, overpaymentSum)
            if (terminalCost) {
                const debt = overpaymentSum - terminalCost
                if (debt < 0) {
                    const { message_id } = await ctx.reply(`${balance}${overpaymentSum}\n${orgName}${name}\n${addressOrg}${address}`, {
                        reply_markup: {
                            inline_keyboard: [[
                                // {text:lang.keyboards.back, callback_data: "LL"},
                                {text:lang.keyboards.back, callback_data: actions.restartMerchant}
                            ]]
                        }
                    })
                    ctx.session.idsOfMessagesMerchant.push(message_id)
                } 
                if (debt >= 0) {
                    ctx.session.merchantStore.setData({
                        key: merchantKeys.chatId,
                        value: ctx.update.message.chat.id
                    })
                    const { message_id } = await ctx.reply(`${balance}${overpaymentSum}\n${orgName}${name}\n${addressOrg}${address}`, {
                        reply_markup: {
                            inline_keyboard: [
                            [{text:lang.keyboards.next, callback_data: actions.createMerchant}]
                        ]
                        }
                    })
                    ctx.session.idsOfMessagesMerchant.push(message_id)
                }
            } else {
                const { message_id } = await ctx.reply(`${balance}${overpaymentSum}\n${orgName}${name}\n${addressOrg}${address} nits problem`, {
                    reply_markup: {
                        inline_keyboard: [
                            [{text:lang.keyboards.back, callback_data: actions.restartMerchant}]
                        ]
                    }
                })
                ctx.session.idsOfMessagesMerchant.push(message_id)
            }
        } else {
            const { message_id } = await ctx.reply('problem with nits')
            ctx.session.idsOfMessagesMerchant.push(message_id)
        }
    }
    merchantScene.command([commands.stop], async ctx => {
        await ctx.deleteMessage()
        await ctx.scene.leave()
        await ctx.reply('exit', Markup
            .keyboard([
                [commands.start]
            ]).resize().oneTime(true)
        )
    })
    merchantScene.action(actions.restartMerchant, async ctx => {
        // ctx.deleteMessage()
        await ctx.scene.enter(scenes.startEnterTin)
    })
    merchantScene.action(actions.createMerchant, async ctx => {
        // ctx.deleteMessage()
        await ctx.scene.enter(scenes.merchantName)
    })
    merchantScene.leave( async ctx => {
        let { idsOfMessagesMerchant } = ctx.session
        idsOfMessagesMerchant.deleteMessages(ctx)
        ctx.session.idsOfMessagesMerchant = []
    })
})


const merchantNameScene = new Scenes.BaseScene(scenes.merchantName)
merchantNameScene.enter(async ctx => {
    const { message_id } = await ctx.reply(name)
    ctx.session.idsOfMessagesMerchant.push(message_id)
    merchantNameScene.command([commands.stop], async ctx => {
        await ctx.deleteMessage()
        await ctx.scene.leave()
        await ctx.reply('bayy', Markup
            .keyboard([
                [commands.start]
            ]).resize().oneTime(true)
        )
    })
    merchantNameScene.on('text', async ctx => {
        ctx.session.idsOfMessagesMerchant.push(ctx.update.message.message_id)
        ctx.session.merchantStore.setData({
            key: merchantKeys.nickName,
            value: ctx.update.message.text
        })
        ctx.scene.enter(scenes.merchantPhone)
    })
    merchantNameScene.on('message', async ctx => ctx.deleteMessage())
    merchantNameScene.leave( async ctx => {
        const { idsOfMessagesMerchant } = ctx.session
        idsOfMessagesMerchant.deleteMessages(ctx)
        ctx.session.idsOfMessagesMerchant = []
    })
})

const merchantPhoneScene = new Scenes.BaseScene(scenes.merchantPhone)
merchantPhoneScene.enter(async ctx => {
    const { message_id } = await ctx.reply(`${phone}\n for example\n98991112233`)
    ctx.session.idsOfMessagesMerchant.push(message_id)
    merchantPhoneScene.on('text', async ctx => {
        const { merchantStore } = ctx.session
        const { text } = ctx.update.message
        if ( text && !isNaN(parseInt(text)) && (text.length === 11)) {
            ctx.session.idsOfMessagesMerchant.push(ctx.update.message.message_id)
            merchantStore.setData({
                key: merchantKeys.userMobile,
                value: ctx.update.message.text 
            })
            ctx.scene.enter(scenes.merchantPassword)
        } else {
            ctx.scene.reenter()
        }
    })
    merchantPhoneScene.command([commands.stop], async ctx => {
        await ctx.deleteMessage()
        await ctx.scene.leave()
        await ctx.reply('bayy', Markup
            .keyboard([
                [commands.start]
            ]).resize().oneTime(true)
        )
    })
    merchantPhoneScene.on('message', ctx => ctx.deleteMessage())
    merchantPhoneScene.leave( async ctx => {
        const { idsOfMessagesMerchant } = ctx.session
        idsOfMessagesMerchant.deleteMessages(ctx)
        ctx.session.idsOfMessagesMerchant = []
    })
})

const merchantPasswordScene = new Scenes.BaseScene(scenes.merchantPassword)
merchantPasswordScene.enter(async ctx => {
    const { message_id } = await ctx.reply(pwd)
    ctx.session.idsOfMessagesMerchant.push(message_id)
    merchantPasswordScene.command([commands.stop], async ctx => {
        await ctx.deleteMessage()
        await ctx.scene.leave()
        await ctx.reply('bayy', Markup
            .keyboard([
                [commands.start]
            ]).resize().oneTime(true)
        )
    })
    merchantPasswordScene.on('text', async ctx => {
        const { merchantStore } = ctx.session
        const { text } = ctx.update.message
        if ( text && !isNaN(parseInt(text)) && (text.length === 6))  {
            ctx.session.idsOfMessagesMerchant.push(ctx.update.message.message_id)
            merchantStore.setData({
                key: merchantKeys.userPasswd,
                value: ctx.update.message.text 
            })
            ctx.scene.enter(scenes.merchantConfirmation)
        } else {
            ctx.scene.reenter()
        }
    })
    merchantPasswordScene.on('message', async ctx => ctx.deleteMessage())
    merchantPasswordScene.leave( async ctx => {
        const { idsOfMessagesMerchant } = ctx.session
        idsOfMessagesMerchant.deleteMessages(ctx)
        ctx.session.idsOfMessagesMerchant = []
    })
})

// const merchantPasFileScene = new Scenes.BaseScene(scenes.merchantPasFile)
// merchantPasFileScene.enter(async ctx => {
//     await ctx.reply(pasFile)
//     merchantPasFileScene.on('document', async ctx => {
//         if (ctx.update.message.document) {
//             merchantStore.setData({
//                 key: merchantKeys.passFile,
//                 value: ctx.update.message.document 
//             })
//             const fileUrl = await ctx.telegram.getFileLink(ctx.update.message.document.file_id);
//             merchantStore.setData({
//                 key: 'href',
//                 value: fileUrl.pathname
//             })
//             ctx.scene.enter(scenes.merchantConfirmation)
//         }
//     })
//     merchantPasFileScene.on('message', async ctx => ctx.reply('send pdf'))
// })
const merchantConfirmationScene = new Scenes.BaseScene(scenes.merchantConfirmation)
merchantConfirmationScene.enter(async ctx => {
    const { data: {
        nickName = undefined,
        userMobile = undefined,
        userPasswd = undefined,
    } } = ctx.session.merchantStore
    const { message_id } = await ctx.reply(
        `${confirm.fullName}${nickName}\n${confirm.phone}${userMobile}\n${confirm.password}${userPasswd}`, {
            reply_markup: {
                inline_keyboard: [
                    [{text:lang.keyboards.back, callback_data: actions.restartMerchant},
                    {text:lang.keyboards.next, callback_data: actions.confirmMerchant}]
                ]
            }
    })
    ctx.session.idsOfMessagesMerchant.push(message_id)
    merchantConfirmationScene.command([commands.stop], async ctx => {
        await ctx.deleteMessage()
        await ctx.scene.leave()
        await ctx.reply('bayy', Markup
            .keyboard([
                [commands.start]
            ]).resize().oneTime(true)
        )
    })
    merchantConfirmationScene.action(actions.restartMerchant, ctx => {
        ctx.scene.enter(scenes.merchantName)
    })
    merchantConfirmationScene.action(actions.confirmMerchant, async ctx => {
        ctx.session.shopStore = {
            data: {},
            setData: function({
                value,
                key = String
            }) {
                this.data[key] = value
            }
        }
        const { idsOfMessagesMerchant } = ctx.session
        idsOfMessagesMerchant.deleteMessages(ctx)
        ctx.session.idsOfMessagesMerchant = []
        ctx.session.shopStore.setData({
            key: shopKeys.tin,
            value: ctx.session.merchantStore.data.tin
        })
            const { success, data } = await onSubmitMerchant(ctx.session.merchantStore.data)
            // delete ctx.session.merchantStore
            if (success && data && data._id && data.merMapId) {
                console.log('datttaafromserverere',data)
                ctx.session.shopStore.setData({
                    key: shopKeys.merchantId,
                    value: data._id
                })
                ctx.session.shopStore.setData({
                    key: shopKeys.merMapId,
                    value: data.merMapId
                })
                await ctx.reply('success sign up merchant')
                ctx.session.merchantStore.data = {} 
                ctx.scene.enter(scenes.shop.enterHome)
            } else {
                ctx.scene.reenter()
                const { message_id } = await ctx.reply('problem with server')
                ctx.session.idsOfMessagesMerchant.push(message_id)
            }
    })
    merchantConfirmationScene.on('message', async ctx => ctx.deleteMessage())
    merchantConfirmationScene.leave( async ctx => {
        const { idsOfMessagesMerchant } = ctx.session
        idsOfMessagesMerchant.deleteMessages(ctx)
        ctx.session.idsOfMessagesMerchant = []
    })
})


// merchantScene.action(actions.merchant.phone, ctx => {
//     ctx.reply(phone)
//     merchantScene.on('text', ctx => {
//         if (ctx.update.message.text) {
//             merchantStore.setData({
//                 key: merchantKeys.userMobile,
//                 value: ctx.update.message.text 
//             })
//         }
//     }) 
// })

module.exports = {
    // merchantEnterScene,
    merchantScene,
    merchantNameScene,
    merchantPhoneScene,
    merchantPasswordScene,
    merchantConfirmationScene,
}