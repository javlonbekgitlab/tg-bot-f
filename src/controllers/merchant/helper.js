const FormData = require("form-data")
const { Api } = require("../../api")
const { apiUrles } = require("../../utils/constants")
const fs = require('fs')

const onGetInfoByTin = async ({tinOrPinfl, chatId}) => {
    const response = {}
    const res = await Api({
        url: `${apiUrles.checkBalanceWithChatId}?tin=${tinOrPinfl}&chatId=${chatId}`
    })
    if (res.status === 200) {
        response.success = true
        response.data = res.data
    } else {
        response.success = false
    }
    return response
}

const onSubmitMerchant = async values => {
    const {
        // passFile,
        nickName,
        userMobile,
        userPasswd,
        tin,
        chatId
        // href
    } = values
    const response = {}
    console.log('values',values)
    try {
        const res = await Api({
            data: {
                nickName,
                userMobile,
                userPasswd,
                tin,
                chatId
            },
            type: 'post',
            url: apiUrles.addMerchant
        })
        response.success = true
        response.data = res.data
    } catch (e) {
        console.log(e)
        response.success = false
    }
    // progressForm.setProgressForm(0)
    return response
}

module.exports = {
    onGetInfoByTin,
    onSubmitMerchant
}