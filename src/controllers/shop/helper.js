const { Api } = require("../../api")
const { apiUrles } = require("../../utils/constants")

const getRegions = async () => {
    try {
        const res = await Api({
            url: `${apiUrles.infoRegions}?lang=${`uz`}`
        })
        return res?.data?.data
    } catch (e) {
        console.log(e)
        return false
    }
}

const getDistricts = async code => {
    const res = await Api({
        url: `${apiUrles.districtsByRegion}?lang=${`uz`}&code=${code}`
    })
    console.log(`${apiUrles.districtsByRegion}?lang=${`uz`}&code=${code}`)
    if (res.status === 200) {
        return res?.data?.data
    } else {
        console.log(e)
        return false
    }
}

const getGroupActivities = async () => {
    try {
        const res = await Api({
            url: `${apiUrles.activityGroup}?lang=${`uz`}`
        })
        return res?.data?.data
    } catch (e) {
        console.log(e)
        return false
    }
}

const getListActivities = async code => {
    try {
        const res = await Api({
            url: `${apiUrles.activityList}?lang=${`uz`}&code=${code}`
        })
        return res?.data?.data
    } catch (e) {
        console.log(e)
        return false
    }
}

const onSubmitShop = async data => {
    // const {
    //     // passFile,
    //     shopName,
    //     merMapId,
    //     cityAddress,
    //     regionCode,
    //     regionName,
    //     districtCode,
    //     districtName,
    //     activityGroup,
    //     activityGroupName,
    //     activityType,
    //     activityTypeName,
    //     cadastreNumber,
    //     fileName,
    //     latitude,
    //     longitude,
    //     tin,
    //     merchantId
    //     // href
    // } = values
    const response = {}
    console.log('a__________________________________________',data)
    const res = await Api({
        data,
        type: 'post',
        url: apiUrles.addStore
    })
    if (res.status === 200) {
        response.success = true
        response.data = res.data
    } else {
        response.data = false
        response.success = false
    }
    // progressForm.setProgressForm(0)
    return response
}

module.exports = {
    getRegions,
    getDistricts,
    getGroupActivities,
    getListActivities,
    onSubmitShop
}
