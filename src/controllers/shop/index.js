const { Scenes, Markup } = require("telegraf")
const { autoSelectName } = require("../../actions")
const { lang } = require("../../lang/lang")
const { actions, commands } = require("../../utils/actions")
// const { shopStore } = require("../../store")
const { scenes } = require("../../utils/constants")
const { shopKeys, merchantKeys } = require("../../utils/keysForStore")
const { getRegions, getDistricts, getGroupActivities, getListActivities, onSubmitShop } = require("./helper")
require('../../actions/deleteHistory.js')
require('../../actions/clearArray.js')

const {
    shopName,
    region,
    district,
    activityGroup,
    activityType,
    cadastreNumber,
    address,
    location,
    cadastreFile,
    confirm,
    addShop
} = lang.dialog.shop

const shopEnterBackScene = new Scenes.BaseScene(scenes.shop.enterBack)
shopEnterBackScene.enter(async ctx => {
    const { message_id } = await ctx.reply(addShop, Markup
        .keyboard([
            [commands.menu]
        ]).resize().oneTime(true)
    )
    ctx.session.idOfCommandButton = [message_id]
    ctx.session.nextScene = scenes.cashDrawer.preBackEnter
    ctx.scene.enter(scenes.shop.shopName)
})

const shopEnterHomeScene = new Scenes.BaseScene(scenes.shop.enterHome)
shopEnterHomeScene.enter(async ctx => {
    const { message_id } = await ctx.reply(addShop, Markup
        .keyboard([
            [commands.stop]
        ]).resize().oneTime(true)
    )
    ctx.session.nextScene = scenes.cashDrawer.preHomeEnter
    ctx.session.idOfCommandButton = [message_id]
    ctx.scene.enter(scenes.shop.shopName)
})

const shopNameScene = new Scenes.BaseScene(scenes.shop.shopName)
shopNameScene.enter(async ctx => {
    // if (!ctx.session.shopStore) {
    //     ctx.session.shopStore = {
    //         data: {},
    //         setData: function({
    //             value,
    //             key = String
    //         }) {
    //             this.data[key] = value
    //         }
    //     }
    // }
    const { message_id } = await ctx.reply(shopName)
    ctx.session.idsOfmessagesShopScene = [message_id]
    shopNameScene.command([commands.stop], async ctx => {
        const { idOfCommandButton } = ctx.session
        await ctx.deleteMessage()
        await idOfCommandButton.deleteMessages(ctx)
        ctx.session.shopStore.data = {}
        ctx.scene.leave()
        ctx.reply('bayy')
    })

    shopNameScene.command([commands.menu], async ctx => {
        const { idOfCommandButton } = ctx.session
        await ctx.deleteMessage()
        await idOfCommandButton.deleteMessages(ctx)
        ctx.session.shopStore.data = {}
        ctx.scene.enter(scenes.merchantMenu.home)
    })

    shopNameScene.on('text', ctx => {
        console.log(ctx.update.message)
        ctx.session.shopStore.setData({
            key: shopKeys.shopName,
            value: ctx.update.message.text 
        })
        ctx.scene.enter(scenes.shop.region)
        ctx.session.idsOfmessagesShopScene.push(ctx.update.message.message_id)
    })
    shopNameScene.on('message', async ctx => ctx.deleteMessage())
    shopNameScene.leave( async ctx => {
        let { idsOfmessagesShopScene } = ctx.session
        idsOfmessagesShopScene.deleteMessages(ctx)
        ctx.session.idsOfmessagesShopScene = []
    })
})

const shopRegionScene = new Scenes.BaseScene(scenes.shop.region)
shopRegionScene.enter(async ctx => {
    ctx.session.regions = await getRegions()
    if (ctx.session.regions) {
        const regionKeyboards = ctx.session.regions.map(item => {
            return [{text: item.name, callback_data: item.code}]
        })
        const { message_id } = await ctx.reply(region, {
            reply_markup: {
                inline_keyboard: regionKeyboards,
                resize_keyboard: true,
                one_time_keyboard: true
            }

        })
        ctx.session.idsOfmessagesShopScene.push(message_id)
        shopRegionScene.on('callback_query', async ctx => {
            console.log('region callback',ctx.update.callback_query.data)
            await ctx.session.shopStore.setData({
                key: shopKeys.regionCode,
                value: ctx.update.callback_query.data
            })
            await ctx.session.shopStore.setData({
                key: shopKeys.regionName,
                value: autoSelectName({
                    data: ctx.session.regions,
                    keyOfValue: 'name',
                    keyOfData: 'code',
                    value: parseInt(ctx.update.callback_query.data)
                })
            })
            ctx.scene.enter(scenes.shop.district)
        })
    } else {
        const { message_id } = await ctx.reply('no connection with server')
        const { idsOfmessagesShopScene } = ctx.session
        idsOfmessagesShopScene.push(message_id)
    }

    shopRegionScene.command([commands.stop], async ctx => {
        await ctx.deleteMessage()
        const { idOfCommandButton } = ctx.session
        await idOfCommandButton.deleteMessages(ctx)
        ctx.session.shopStore.data = {}
        ctx.scene.leave()
        ctx.reply('bay')
    })

    shopRegionScene.command([commands.menu], async ctx => {
        await ctx.deleteMessage()
        const { idOfCommandButton } = ctx.session
        await idOfCommandButton.deleteMessages(ctx)
        ctx.session.shopStore.data = {}
        ctx.scene.enter(scenes.merchantMenu.home)
    })
    shopRegionScene.on('message', async ctx => ctx.deleteMessage())
    shopRegionScene.leave( async ctx => {
        let { idsOfmessagesShopScene } = ctx.session
        await idsOfmessagesShopScene.deleteMessages(ctx)
        ctx.session.idsOfmessagesShopScene = []
        delete ctx.session.regions
    })
})

const shopDistrictScene = new Scenes.BaseScene(scenes.shop.district)
shopDistrictScene.enter(async ctx => {
    const { regionCode } = ctx.session.shopStore.data
    console.log('regionCode',regionCode)
    if (regionCode) {
        ctx.session.districts = await getDistricts(regionCode)
        if (ctx.session.districts) {
            const districtKeyboards = ctx.session.districts.map(item => {
                return [{text: item.name, callback_data: item.code}]
            })
            const { message_id } = await ctx.reply(district, {
                reply_markup: {
                    inline_keyboard: districtKeyboards,
                },
            })
            ctx.session.idsOfmessagesShopScene.push(message_id)
            shopDistrictScene.on('callback_query', async ctx => {
                console.log('district',ctx.update.callback_query.data)
                await ctx.session.shopStore.setData({
                    key: shopKeys.districtCode,
                    value: ctx.update.callback_query.data
                })
                console.log(ctx.session.districts)
                console.log('query',ctx.update.callback_query.data)
                await ctx.session.shopStore.setData({
                    key: shopKeys.districtName,
                    value: autoSelectName({
                        data: ctx.session.districts,
                        keyOfValue: 'name',
                        keyOfData: 'code',
                        value: parseInt(ctx.update.callback_query.data)
                    })
                })
                ctx.scene.enter(scenes.shop.activityGroup)
            })
        } else  {
            const { message_id } = await ctx.reply('no connection with server')
            const { idsOfmessagesShopScene } = ctx.session
            idsOfmessagesShopScene.push(message_id)
        }
    
    } else {
        ctx.reply('no regionCode')
        console.log('no regionCode')
    }
    shopDistrictScene.command([commands.stop], async ctx => {
        await ctx.deleteMessage()
        const { idOfCommandButton } = ctx.session
        await idOfCommandButton.deleteMessages(ctx)
        ctx.session.shopStore.data = {}
        ctx.scene.leave()
        ctx.reply('bay')
    })

    shopDistrictScene.command([commands.menu], async ctx => {
        await ctx.deleteMessage()
        const { idOfCommandButton } = ctx.session
        await idOfCommandButton.deleteMessages(ctx)
        ctx.session.shopStore.data = {}
        ctx.scene.enter(scenes.merchantMenu.home)
    })
    shopDistrictScene.on('message', async ctx => ctx.deleteMessage())
    shopDistrictScene.leave( async ctx => {
        let { idsOfmessagesShopScene } = ctx.session
        await idsOfmessagesShopScene.deleteMessages(ctx)
        ctx.session.idsOfmessagesShopScene = []
        delete ctx.session.districts
    })
})


const shopActivityGroupScene = new Scenes.BaseScene(scenes.shop.activityGroup)
shopActivityGroupScene.enter(async ctx => {
    ctx.session.activityGroups = await getGroupActivities()
    if (ctx.session.activityGroups) {
        const activityGroupKeyboards = ctx.session.activityGroups.map(item => {
            return [{text: item.name, callback_data: item.code}]
        })
        const { message_id } = await ctx.reply(activityGroup, {
            reply_markup: {
                inline_keyboard: activityGroupKeyboards,
            },
        })
        ctx.session.idsOfmessagesShopScene.push(message_id)
        shopActivityGroupScene.on('callback_query', async ctx => {
            console.log(ctx.session.activityGroups)
            console.log('group act',ctx.update.callback_query.data)

            await ctx.session.shopStore.setData({
                key: shopKeys.activityGroup,
                value: ctx.update.callback_query.data
            })
            await ctx.session.shopStore.setData({
                key: shopKeys.activityGroupName,
                value: autoSelectName({
                    data: ctx.session.activityGroups,
                    keyOfValue: 'name',
                    keyOfData: 'code',
                    value: parseInt(ctx.update.callback_query.data)
                })
            })
            ctx.scene.enter(scenes.shop.activityType)
        })
    } else {
        const { message_id } = await ctx.reply('no connection with server')
        const { idsOfmessagesShopScene } = ctx.session
        idsOfmessagesShopScene.push(message_id)
    }

    shopActivityGroupScene.command([commands.stop], async ctx => {
        await ctx.deleteMessage()
        const { idOfCommandButton } = ctx.session
        await idOfCommandButton.deleteMessages(ctx)
        ctx.session.shopStore.data = {}
        ctx.scene.leave()
        ctx.reply('bay')
    })

    shopActivityGroupScene.command([commands.menu], async ctx => {
        await ctx.deleteMessage()
        const { idOfCommandButton } = ctx.session
        await idOfCommandButton.deleteMessages(ctx)
        ctx.session.shopStore.data = {}
        ctx.scene.enter(scenes.merchantMenu.home)
    })
    shopActivityGroupScene.on('message', async ctx => ctx.deleteMessage())
    shopActivityGroupScene.leave( async ctx => {
        let { idsOfmessagesShopScene } = ctx.session
        await idsOfmessagesShopScene.deleteMessages(ctx)
        ctx.session.idsOfmessagesShopScene = []
        delete ctx.session.activityGroups
    })
})


const shopActivityTypeScene = new Scenes.BaseScene(scenes.shop.activityType)
shopActivityTypeScene.enter(async ctx => {
    const { activityGroup } = ctx.session.shopStore.data
    if (activityGroup) {
        ctx.session.activityTypes = await getListActivities(activityGroup)
        if (ctx.session.activityTypes) {
            const activityTypeKeyboards = ctx.session.activityTypes.map(item => {
                return [{text: item.name, callback_data: item.code}]
            })
            const { message_id } = await ctx.reply(activityType, {
                reply_markup: {
                    inline_keyboard: activityTypeKeyboards,
                },
            })
            console.log(ctx.session.activityTypes)
            ctx.session.idsOfmessagesShopScene.push(message_id)
            shopActivityTypeScene.on('callback_query', async ctx => {
                console.log('query type',ctx.update.callback_query.data )
                await ctx.session.shopStore.setData({
                    key: shopKeys.activityType,
                    value: ctx.update.callback_query.data
                })
                await ctx.session.shopStore.setData({
                    key: shopKeys.activityTypeName,
                    value: autoSelectName({
                        data: ctx.session.activityTypes,
                        keyOfValue: 'name',
                        keyOfData: 'code',
                        value: parseInt(ctx.update.callback_query.data)
                    })
                })
                ctx.scene.enter(scenes.shop.cadastre)
            })
        } else {
            const { message_id } = await ctx.reply('no connection with server')
            const { idsOfmessagesShopScene } = ctx.session
            idsOfmessagesShopScene.push(message_id)
        }
    } else {
        ctx.reply('no activity grouipcocded')
        console.log('activity no grouipcocded')
    }
    shopActivityTypeScene.command([commands.stop], async ctx => {
        await ctx.deleteMessage()
        const { idOfCommandButton } = ctx.session
        await idOfCommandButton.deleteMessages(ctx)
        ctx.session.shopStore.data = {}
        ctx.scene.leave()
        ctx.reply('bay')
    })

    shopActivityTypeScene.command([commands.menu], async ctx => {
        await ctx.deleteMessage()
        const { idOfCommandButton } = ctx.session
        await idOfCommandButton.deleteMessages(ctx)
        ctx.session.shopStore.data = {}
        ctx.scene.enter(scenes.merchantMenu.home)
    })
    shopActivityTypeScene.on('message', async ctx => ctx.deleteMessage())
    shopActivityTypeScene.leave( async ctx => {
        let { idsOfmessagesShopScene } = ctx.session
        await idsOfmessagesShopScene.deleteMessages(ctx)
        ctx.session.idsOfmessagesShopScene = []
        delete ctx.session.activityTypes
    })
})

const shopCadastreScene = new Scenes.BaseScene(scenes.shop.cadastre)
shopCadastreScene.enter(async ctx => {
    const { message_id } = await ctx.reply(cadastreNumber)
    ctx.session.idsOfmessagesShopScene.push(message_id)
    shopCadastreScene.command([commands.stop], async ctx => {
        await ctx.deleteMessage()
        const { idOfCommandButton } = ctx.session
        await idOfCommandButton.deleteMessages(ctx)
        ctx.session.shopStore.data = {}
        ctx.scene.leave()
        ctx.reply('bay')
    })

    shopCadastreScene.command([commands.menu], async ctx => {
        await ctx.deleteMessage()
        const { idOfCommandButton } = ctx.session
        await idOfCommandButton.deleteMessages(ctx)
        ctx.session.shopStore.data = {}
        ctx.scene.enter(scenes.merchantMenu.home)
    })
    shopCadastreScene.on('text', ctx => {
        ctx.session.shopStore.setData({
            key: shopKeys.cadastreNumber,
            value: ctx.update.message.text 
        })
        ctx.session.idsOfmessagesShopScene.push(ctx.update.message.message_id)
        ctx.scene.enter(scenes.shop.address)
    })
    shopCadastreScene.on('message', async ctx => ctx.deleteMessage())
    shopCadastreScene.leave( async ctx => {
        let { idsOfmessagesShopScene } = ctx.session
        await idsOfmessagesShopScene.deleteMessages(ctx)
        ctx.session.idsOfmessagesShopScene = []
    })
})

const shopAddressScene = new Scenes.BaseScene(scenes.shop.address)
shopAddressScene.enter(async ctx => {
    const { message_id } = await ctx.reply(address)
    ctx.session.idsOfmessagesShopScene.push(message_id)
    shopAddressScene.command([commands.stop], async ctx => {
        await ctx.deleteMessage()
        const { idOfCommandButton } = ctx.session
        await idOfCommandButton.deleteMessages(ctx)
        ctx.session.shopStore.data = {}
        ctx.scene.leave()
        ctx.reply('bay')
    })

    shopAddressScene.command([commands.menu], async ctx => {
        await ctx.deleteMessage()
        const { idOfCommandButton } = ctx.session
        await idOfCommandButton.deleteMessages(ctx)
        ctx.session.shopStore.data = {}
        ctx.scene.enter(scenes.merchantMenu.home)
    })
    shopAddressScene.on('text', ctx => {
        ctx.session.shopStore.setData({
            key: shopKeys.cityAddress,
            value: ctx.update.message.text 
        })
        ctx.session.idsOfmessagesShopScene.push(ctx.update.message.message_id)
        ctx.scene.enter(scenes.shop.location)
    })
    shopAddressScene.on('message', async ctx => ctx.deleteMessage())
    shopAddressScene.leave( async ctx => {
        let { idsOfmessagesShopScene } = ctx.session
        await idsOfmessagesShopScene.deleteMessages(ctx)
        ctx.session.idsOfmessagesShopScene = []
    })
})

const shopLocationScene = new Scenes.BaseScene(scenes.shop.location)
shopLocationScene.enter(async ctx => {
    const { message_id } = await ctx.reply(location)
    ctx.session.idsOfmessagesShopScene.push(message_id)
    shopLocationScene.command([commands.stop], async ctx => {
        await ctx.deleteMessage()
        const { idOfCommandButton } = ctx.session
        await idOfCommandButton.deleteMessages(ctx)
        ctx.session.shopStore.data = {}
        ctx.scene.leave()
        ctx.reply('bay')
    })

    shopLocationScene.command([commands.menu], async ctx => {
        await ctx.deleteMessage()
        const { idOfCommandButton } = ctx.session
        await idOfCommandButton.deleteMessages(ctx)
        ctx.session.shopStore.data = {}
        ctx.scene.enter(scenes.merchantMenu.home)
    })
    shopLocationScene.on('location', ctx => {
        console.log(ctx)
        ctx.session.shopStore.setData({
            key: shopKeys.latitude,
            value: ctx.update.message.location.latitude 
        })
        ctx.session.shopStore.setData({
            key: shopKeys.longitude,
            value: ctx.update.message.location.longitude 
        })
        ctx.session.idsOfmessagesShopScene.push(ctx.update.message.message_id)
        ctx.scene.enter(scenes.shop.cadastreFile)
    })
    shopLocationScene.on('message', async ctx => ctx.deleteMessage())
    shopLocationScene.leave( async ctx => {
        let { idsOfmessagesShopScene } = ctx.session
        await idsOfmessagesShopScene.deleteMessages(ctx)
        ctx.session.idsOfmessagesShopScene = []
    })
})

const shopFileScene = new Scenes.BaseScene(scenes.shop.cadastreFile)
shopFileScene.enter(async ctx => {
    const { message_id } = await ctx.reply(cadastreFile)
    ctx.session.idsOfmessagesShopScene.push(message_id)
    shopFileScene.command([commands.stop], async ctx => {
        await ctx.deleteMessage()
        const { idOfCommandButton } = ctx.session
        await idOfCommandButton.deleteMessages(ctx)
        ctx.session.shopStore.data = {}
        ctx.scene.leave()
        ctx.reply('bay')
    })

    shopFileScene.command([commands.menu], async ctx => {
        await ctx.deleteMessage()
        const { idOfCommandButton } = ctx.session
        await idOfCommandButton.deleteMessages(ctx)
        ctx.session.shopStore.data = {}
        ctx.scene.enter(scenes.merchantMenu.home)
    })
    shopFileScene.on('document', async ctx => {
        const fileUrl = await ctx.telegram.getFileLink(ctx.update.message.document.file_id);
        ctx.session.shopStore.setData({
            key: shopKeys.cadastreFile,
            value: fileUrl.href 
        })
        console.log(ctx.session.shopStore.data)
        ctx.session.idsOfmessagesShopScene.push(ctx.update.message.message_id)
        ctx.scene.enter(scenes.shop.confirmation)
    })
    shopFileScene.on('message', async ctx => ctx.deleteMessage())
    shopFileScene.leave( async ctx => {
        let { idsOfmessagesShopScene } = ctx.session
        await idsOfmessagesShopScene.deleteMessages(ctx)
        ctx.session.idsOfmessagesShopScene = []
    })
})

const shopConfirmationScene = new Scenes.BaseScene(scenes.shop.confirmation)
shopConfirmationScene.enter( async ctx => {
    const {
        shopName = undefined,
        cityAddress = undefined,
        regionName = undefined,
        districtName = undefined,
        activityGroupName = undefined,
        activityTypeName = undefined,
        cadastreNumber = undefined,
    } = ctx.session.shopStore?.data
    const { message_id } = await ctx.reply(
        `${confirm.shopName}${shopName}\n` +
        `${confirm.region}${regionName}\n` +
        `${confirm.district}${districtName}\n` +
        `${confirm.activityGroup}${activityGroupName}\n` +
        `${confirm.activityType}${activityTypeName}\n` +
        `${confirm.cadastreNumber}${cadastreNumber}\n` +
        `${confirm.address}${cityAddress}`, {
            reply_markup: {
                inline_keyboard: [
                    [{text:lang.keyboards.back, callback_data: actions.restartShop},
                    {text:lang.keyboards.next, callback_data: actions.confirmShop}]
                ]
            }
    })
    ctx.session.idsOfmessagesShopScene.push(message_id)
    shopConfirmationScene.command([commands.stop], async ctx => {
        await ctx.deleteMessage()
        const { idOfCommandButton } = ctx.session
        await idOfCommandButton.deleteMessages(ctx)
        ctx.session.shopStore.data = {}
        ctx.scene.leave()
        ctx.reply('bay')
    })

    shopConfirmationScene.command([commands.menu], async ctx => {
        await ctx.deleteMessage()
        const { idOfCommandButton } = ctx.session
        await idOfCommandButton.deleteMessages(ctx)
        ctx.session.shopStore.data = {}
        ctx.scene.enter(scenes.merchantMenu.home)
    })
    shopConfirmationScene.action(actions.restartShop, ctx => {
        console.log(ctx.session.shopStore.data)
        ctx.scene.enter(scenes.shop.shopName)
    })
    shopConfirmationScene.action(actions.confirmShop, async ctx => {
        const { success, data } = await onSubmitShop(ctx.session.shopStore.data)
        console.log('response from server shop ', data)
        if (success) {
            const { _id, shopMapId } = data
            console.log(_id, shopMapId)
            ctx.reply('success shop sign up')
            if (_id && shopMapId) {
                if (!ctx.session.cashDrawerStore) {
                    ctx.session.cashDrawerStore = {
                        data: {},
                        setData: function({
                            value,
                            key = String
                        }) {
                            this.data[key] = value
                        }
                    }
                }
                ctx.session.cashDrawerStore.setData({
                    key: shopKeys.shopId,
                    value: _id
                })
                ctx.session.cashDrawerStore.setData({
                    key: shopKeys.shopMapId,
                    value: shopMapId
                })
                ctx.session.cashDrawerStore.setData({
                    key: shopKeys.merchantId,
                    value: ctx.session.shopStore.data.merchantId
                })
                ctx.session.cashDrawerStore.setData({
                    key: shopKeys.tin,
                    value: ctx.session.shopStore.data.tin
                })
                const { idOfCommandButton } = ctx.session
                await idOfCommandButton.deleteMessages(ctx)
                ctx.scene.enter(ctx.session.nextScene)
                delete ctx.session.nextScene
            } else {
                ctx.reply('no shopId or shopMapId')
            }
        } else {
            ctx.reply('not success shop sign up')
        }
    })
    shopConfirmationScene.on('message', async ctx => ctx.deleteMessage())
    shopConfirmationScene.leave( async ctx => {
        let { idsOfmessagesShopScene } = ctx.session
        await idsOfmessagesShopScene.deleteMessages(ctx)
        ctx.session.idsOfmessagesShopScene = []
        const { merMapId ,tin, merchantId } = shopKeys
        const merchantDataKeys = [merMapId, tin, merchantId]
        const shopStoreKeys = Object.keys(ctx.session.shopStore.data)
        for (let index = 0; index < shopStoreKeys.length; index ++) {
            if (!(merchantDataKeys.includes(shopStoreKeys[index]))){
                delete ctx.session.shopStore.data[shopStoreKeys[index]]
            }
        }
    })
})
// {
//     tin: 305130412,
//     merchantId: '61000c9cac4e976b256fc9d0',
//     merMapId: '60052281',
//     shopName: 'asd',
//     regionCode: '35',
//     regionName: 'ҚОРАҚАЛПОҒИСТОН РЕСП.',
//     districtCode: '23',
//     districtName: 'ТАХИАТОШ ТУМАНИ',
//     activityGroup: '9',
//     activityGroupName: 'Бошка фаолият турлари',
//     activityType: '1',
//     activityTypeName: 'ЙУК',
//     cadastreNumber: '5',
//     cityAddress: 'asdsa',
//     latitude: 41.453519,
//     longitude: 69.393534,
//     fileName: 'https://api.telegram.org/file/bot1888800496:AAGbRy8w8mBdkgulaHOtNAEbaYiC1lbGvH4/documents/file_74.odt'
// }
  
module.exports = {
    shopEnterBackScene,
    shopEnterHomeScene,
    shopNameScene,
    shopRegionScene,
    shopDistrictScene,
    shopActivityGroupScene,
    shopActivityTypeScene,
    shopCadastreScene,
    shopAddressScene,
    shopLocationScene,
    shopFileScene,
    shopConfirmationScene
}


