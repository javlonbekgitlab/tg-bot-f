const { Scenes, Markup } = require("telegraf");
const { checkBalance } = require("../../actions");
const { lang } = require("../../lang/lang");
const { commands } = require("../../utils/actions");
const { scenes } = require("../../utils/constants");
const { shopKeys } = require("../../utils/keysForStore");
const { getShops, getCashDrawers, onUploadContractFile } = require("./helper");
require('../../actions/deleteHistory.js')

const {
    revice,
    welcome,
    chooseSection,
    menu,
    hello,
    listCashDrawers,
    listShops,
    addContractFile,
    uploadContractFile,
    messageUploadedFile,
    addShop,
    faq,
    exit
} = lang.dialog.merchantMenu

const merchantHomeScene = new Scenes.BaseScene(scenes.merchantMenu.home)
merchantHomeScene.enter( async ctx => {
    const repliedMarkup = await ctx.reply(welcome, Markup
        .keyboard([
            [commands.exit]
        ]).resize().oneTime(true)
    )
    // ctx.editMessageReplyMarkup(ctx.chat.id, repliedMarkup.message_id, null, Markup
    //     .keyboard([
    //         ['commands.exit']
    //     ]).resize().oneTime(true))
    const repliedMessage = await ctx.reply(chooseSection, {
        reply_markup: {
            inline_keyboard: [
                [{text: revice, callback_data: scenes.merchantMenu.revice}],
                [{text: listShops, callback_data: scenes.merchantMenu.listShops}],
                [{text: listCashDrawers, callback_data: scenes.merchantMenu.listCashDrawers}],
                [{text: addShop, callback_data: scenes.merchantMenu.addShop}],
                [{text: faq, callback_data: scenes.faq.preBack}],
                // [{text: exit, callback_data: scenes.merchantMenu.exit}]
            ]
        }
    })
    ctx.session.idsOfmessagesMerchantHome = [repliedMarkup.message_id, repliedMessage.message_id]
    merchantHomeScene.command([commands.exit], async ctx => {
        await ctx.deleteMessage()
        ctx.scene.leave()
        ctx.reply('exit', Markup
        .keyboard([
            [commands.start]
        ]).resize().oneTime(true)
    )
    })
    merchantHomeScene.on('callback_query', async ctx => {
        ctx.scene.enter(ctx.update.callback_query.data)
    })
    merchantHomeScene.on('message', ctx => ctx.deleteMessage())
    merchantHomeScene.leave( async ctx => {
        let { idsOfmessagesMerchantHome } = ctx.session
        await idsOfmessagesMerchantHome.deleteMessages(ctx)
        ctx.session.idsOfmessagesMerchantHome = []
    })
})

const merchantReviceScene = new Scenes.BaseScene(scenes.merchantMenu.revice)
merchantReviceScene.enter( async ctx => {
    const { message_id } = await ctx.reply(revice, Markup
        .keyboard([
            [commands.back]
        ]).resize().oneTime(true)
    )
    ctx.session.idsOfmessagesMerchantRevice = [message_id]
    merchantReviceScene.command([commands.back], async ctx => {
        await ctx.deleteMessage()
        const { idsOfmessagesMerchantRevice } = ctx.session
        await idsOfmessagesMerchantRevice.deleteMessages(ctx)
        delete ctx.session.idsOfmessagesMerchantRevice 
        ctx.scene.enter(scenes.merchantMenu.home)
    })
})

const merchantListShopsScene = new Scenes.BaseScene(scenes.merchantMenu.listShops)
merchantListShopsScene.enter(async ctx => {
    const { message_id } = await ctx.reply(lang.dialog.merchantMenu.listShops, Markup
        .keyboard([
            [commands.back]
        ]).resize().oneTime(true)
    )
    ctx.session.idsOfmessages = [message_id]
    const { idsOfmessages } = ctx.session
    const { confirm } = lang.dialog.shop
    const { merchantId } = ctx.session?.merchantStore?.data
    console.log(ctx.session.merchantStore.data)
    if (merchantId) {
        const { success, data } = await getShops(merchantId)
        console.log( data)
        if (success) {
            for (let ittr = 0; ittr < data.length; ittr ++) {
                const repliedMessage = await ctx.reply(
                    `${confirm.shopName}${data[ittr].shopName}\n` +
                    `${confirm.region}${data[ittr].regionName}\n` +
                    `${confirm.district}${data[ittr].districtName}\n` +
                    `${confirm.activityGroup}${data[ittr].activityGroupName}\n` +
                    `${confirm.activityType}${data[ittr].activityTypeName}\n` +
                    `${confirm.cadastreNumber}${data[ittr].cadastreNumber}\n` +
                    `${confirm.address}${data[ittr].cityAddress}`, {
                        reply_markup: {
                            inline_keyboard: [
                                [{text:lang.dialog.merchantMenu.addCashDrawer, callback_data: JSON.stringify({
                                    shopId: data[ittr]._id,
                                    shopMapId: data[ittr].shopMapId
                                })}]
                            ]
                        }
                })
                idsOfmessages.push(repliedMessage.message_id)
            }  
        } else {
            ctx.reply('problem with server connection')
        }
    } else {
        ctx.reply('no merchant id')
    }

    merchantListShopsScene.command([commands.back], async ctx => {
        await ctx.deleteMessage()
        const { idsOfmessages } = ctx.session
        await idsOfmessages.deleteMessages(ctx)
        ctx.scene.enter(scenes.merchantMenu.home)
    })

    merchantListShopsScene.on('callback_query', async ctx => {
        const shopData = JSON.parse(ctx.update.callback_query.data)
        const { idsOfmessages } = ctx.session
        await idsOfmessages.deleteMessages(ctx)
        if (ctx.session?.merchantStore?.data) {
            const { tin, merchantId } = ctx.session?.merchantStore?.data
            shopData.tin = tin
            shopData.merchantId = merchantId
            if (!ctx.session.cashDrawerStore) {
                ctx.session.cashDrawerStore = {
                    data: {},
                    setData: function({
                        value,
                        key = String
                    }) {
                        this.data[key] = value
                    }
                }
            }
            Object.keys(shopData).map(key => {
                ctx.session.cashDrawerStore.setData({
                    key: shopKeys[key],
                    value: shopData[key]
                })
            })
            console.log('ctx.session.cashDrawerStore. data',ctx.session.cashDrawerStore.data)
            // ctx.session.cashDrawerStore.setData({
            //     key: shopKeys.shopMapId,
            //     value: shopData.shopMapId
            // })
            // ctx.session.cashDrawerStore.setData({
            //     key: shopKeys.tin,
            //     value: tin
            // })
            // ctx.session.cashDrawerStore.setData({
            //     key: shopKeys.merchantId,
            //     value: merchantId
            // })
            const res = await checkBalance(tin)
            if (res === 1) {
                ctx.scene.enter(scenes.cashDrawer.preBackEnter)
            }
            if (res === -1) {
                ctx.scene.enter(scenes.merchantMenu.noMoney)
            } 
            if (res === 0) {
                ctx.scene.enter(scenes.merchantMenu.noConnection)
            }
        } else {
            ctx.reply('no merchant data')
        }
    })
    merchantListShopsScene.on('message', ctx => ctx.deleteMessage())
    // merchantListShopsScene.leave(ctx => {
    //     idsOfmessages = []
    // })
})

const merchantListCashDrawersScene = new Scenes.BaseScene(scenes.merchantMenu.listCashDrawers)
merchantListCashDrawersScene.enter(async ctx => {
    const repliedTitle = await ctx.reply(lang.dialog.merchantMenu.listCashDrawers, Markup
        .keyboard([
            [commands.back]
        ]).resize().oneTime(true)
    )
    ctx.session.idsOfmessagesCashDrawerList = [repliedTitle.message_id]
    const { idsOfmessagesCashDrawerList } = ctx.session
    const { confirm } = lang.dialog.shop
    const { fiskalMod, serialNumber, modelTitle } = lang.dialog.cashDrawer
    const { merchantId } = ctx.session?.merchantStore?.data
    if (merchantId) {
        const { success, data } = await getCashDrawers(merchantId)
        if (success) {
            console.log(data)  
            for (let ittr = 0; ittr < data.length; ittr ++) {
                const repliedMessage = await ctx.reply(
                    `№ ${ittr + 1} \n` +
                    `${confirm.shopName}${data[ittr].shopName}\n` +
                    `${modelTitle}${data[ittr].modelNo}\n` +
                    `${serialNumber}${data[ittr].machineNo}\n` +
                    `${fiskalMod}${data[ittr].fiskalMod}\n` , (data[ittr]?.status === 0) ? {
                        reply_markup: {
                            inline_keyboard: [[{text: addContractFile, callback_data: data[ittr].id}]]
                        }
                    }: false
                )
                idsOfmessagesCashDrawerList.push(repliedMessage.message_id)
            }  
        } else {
            ctx.reply('problem with server connection')
        }
    } else {
        ctx.reply('no merchant id')
    }
    merchantListCashDrawersScene.on('callback_query', ctx => {
        ctx.session.cashDrawerIdForUploadFile = ctx.update.callback_query.data
        ctx.scene.enter(scenes.merchantMenu.cashDrawerUploadFile)
    })
    merchantListCashDrawersScene.command([commands.back], ctx => {
        ctx.deleteMessage()
        ctx.scene.enter(scenes.merchantMenu.home)
    })
    merchantListCashDrawersScene.on('message', ctx => ctx.deleteMessage())
    merchantListCashDrawersScene.leave( async ctx => {
        const { idsOfmessagesCashDrawerList } = ctx.session
        await idsOfmessagesCashDrawerList.deleteMessages(ctx)
        delete ctx.session.idsOfmessagesCashDrawerList
    })
})

const merchantCashDrawerUploadFileScene = new Scenes.BaseScene(scenes.merchantMenu.cashDrawerUploadFile)
merchantCashDrawerUploadFileScene.enter( async ctx => {
    const { message_id } = await ctx.reply(uploadContractFile, Markup
        .keyboard([
            [commands.back]
        ]).resize().oneTime(true))
    ctx.session.idsOfmessagesCashDrawerUploadFile = [message_id]
    merchantCashDrawerUploadFileScene.on('document', async ctx => {
        const fileUrl = await ctx.telegram.getFileLink(ctx.update.message.document.file_id)
        const { success, data } = await onUploadContractFile({
            id: ctx.session.cashDrawerIdForUploadFile,
            fileName: fileUrl.href
        })
        console.log('upload contract file',data)
        if (success) {
            await ctx.reply(messageUploadedFile)
            ctx.scene.enter(scenes.merchantMenu.home)
            delete ctx.session.cashDrawerIdForUploadFile
        } else {
            ctx.reply('not succes upload file')
            ctx.scene.reenter()
        }
        // ctx.session.shopStore.setData({
        //     key: shopKeys.cadastreFile,
        //     value: fileUrl.href 
        // })
    })

    merchantCashDrawerUploadFileScene.command([commands.back], async ctx => {
        await ctx.deleteMessage()
        ctx.scene.enter(scenes.merchantMenu.listCashDrawers)
    })
    merchantCashDrawerUploadFileScene.on('message', ctx => ctx.deleteMessage())
    merchantCashDrawerUploadFileScene.leave( async ctx => {
        const { idsOfmessagesCashDrawerUploadFile } = ctx.session
        await idsOfmessagesCashDrawerUploadFile.deleteMessages(ctx)
        delete ctx.session.idsOfmessagesCashDrawerUploadFile
    })
})

const merchantAddShopScene = new Scenes.BaseScene(scenes.merchantMenu.addShop)
merchantAddShopScene.enter( async ctx => {
    merchantAddShopScene.command([commands.home], ctx => {
        ctx.scene.leave()
        ctx.reply('bayy', Markup
            .keyboard([
                [commands.start]
            ]).resize().oneTime(true)
        )
    })
    const { merchantId, tin, merMapId } = ctx.session?.merchantStore?.data
    ctx.session.shopStore = {
        data: {},
        setData: function({
            value,
            key = String
        }) {
            this.data[key] = value
        }
    }
    if (merchantId && tin && merMapId) {
        ctx.session.shopStore.setData({
            key: shopKeys.tin,
            value: tin
        })
        ctx.session.shopStore.setData({
            key: shopKeys.merchantId,
            value: merchantId
        })
        ctx.session.shopStore.setData({
            key: shopKeys.merMapId,
            value: merMapId
        })
        const res = await checkBalance(tin)
        if (res === 1) {
            ctx.scene.enter(scenes.shop.enterBack)
        }
        if (res === -1) {
            ctx.scene.enter(scenes.merchantMenu.noMoney)
        } 
        if (res === 0) {
            ctx.scene.enter(scenes.merchantMenu.noConnection)
        }
    } else {
        ctx.reply('no merchant data')
    }
})

const merchantMenuExitScene = new Scenes.BaseScene(scenes.merchantMenu.exit)
merchantMenuExitScene.enter( async ctx => {
    const repliedMessage = await ctx.reply(`exit menu`, Markup
        .keyboard([
            [commands.start]
        ]).resize().oneTime(true)
    )
    // ctx.deleteMessage(repliedMessage.message_id)
})


const merchantMenuNotEnoughMoneyScene = new Scenes.BaseScene(scenes.merchantMenu.noMoney)
merchantMenuNotEnoughMoneyScene.enter( async ctx => {
    console.log('no money')
    const { message_id } = await ctx.reply(`no money`, Markup
        .keyboard([
            [commands.back]
        ]).resize().oneTime(true)
    )
    ctx.session.idOfNotEnoughMoney = [message_id]
    merchantMenuNotEnoughMoneyScene.command([commands.back], async ctx => {
        await ctx.deleteMessage()
        await ctx.scene.enter(scenes.merchantMenu.home)
    })
    merchantMenuNotEnoughMoneyScene.leave( async ctx => {
        await ctx.session.idOfNotEnoughMoney.deleteMessages(ctx)
        delete ctx.session.idOfNotEnoughMoney
    })
    // ctx.deleteMessage(repliedMessage.message_id)
})

const merchantMenuNoConnectionScene = new Scenes.BaseScene(scenes.merchantMenu.noConnection)
merchantMenuNoConnectionScene.enter( async ctx => {
    const { message_id } = await ctx.reply(`no connection`, Markup
        .keyboard([
            [commands.back]
        ]).resize().oneTime(true)
    )
    ctx.session.idOfNoConnection = [message_id]
    merchantMenuNoConnectionScene.command([commands.back], async ctx => {
        await ctx.deleteMessage()
        await ctx.scene.enter(scenes.merchantMenu.home)
    })
    merchantMenuNoConnectionScene.leave( async ctx => {
        await ctx.session.idOfNoConnection.deleteMessages(ctx)
        delete ctx.session.idOfNoConnection
    })
    // ctx.deleteMessage(repliedMessage.message_id)
})


module.exports = { 
    merchantHomeScene,    
    merchantListShopsScene,
    merchantReviceScene,
    merchantAddShopScene,
    merchantListCashDrawersScene,
    merchantCashDrawerUploadFileScene,
    merchantMenuExitScene,
    merchantMenuNoConnectionScene,
    merchantMenuNotEnoughMoneyScene
}
