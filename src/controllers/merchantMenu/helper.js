const { Api } = require("../../api")
const { apiUrles } = require("../../utils/constants")

const getShops = async id => {
    const response = {}
    const res = await Api({ url: `${apiUrles.stores}${id}`})
    if (res.status === 200) {
        response.success = true
        response.data = res.data
    } else {
        response.success = false
    }
    return response
}

const getOneShop = async id => {
    const response = {}
    const res = await Api({
        url: `${apiUrles.oneStore}${id}`
    })
        console.log(res)
    if (res.status === 200) {
        response.success = true
        response.data = res.data
    } else {
        response.success = false
    }
    return response
}

const getCashDrawers = async id => {
    const response = {}
    const res = await Api({ url: `${apiUrles.cashDrawersOfMerchant}${id}`})
    if (res.status === 200) {
        response.success = true
        response.data = res.data
    } else {
        response.success = false
    }
    return response
}

const onUploadContractFile = async data => {
    const response = {}
    console.log('a__________________________________________',data)
    const res = await Api({
        data,
        type: 'post',
        url: apiUrles.uploadContractFile
    })
    if (res.status === 200) {
        response.success = true
        response.data = res.data
    } else {
        response.data = false
        response.success = false
    }
    return response
}


module.exports = {
    getShops,
    getOneShop,
    getCashDrawers,
    onUploadContractFile
}