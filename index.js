const { Telegraf, Scenes, session } = require('telegraf')
const { merchantScene, merchantNameScene, merchantPhoneScene, merchantPasswordScene, merchantConfirmationScene } = require('./src/controllers/merchant')
const { startScene, startEnterTinScene } = require('./src/controllers/start')
const { asyncWrapper } = require('./src/utils/error-handlers')
const { shopNameScene, shopRegionScene, shopDistrictScene, shopActivityGroupScene, shopActivityTypeScene, shopCadastreScene, shopAddressScene, shopLocationScene, shopFileScene, shopConfirmationScene, shopEnterBackScene, shopEnterHomeScene } = require('./src/controllers/shop')
const { scenes } = require('./src/utils/constants')
// const { merchantStore } = require('./src/store')
const { cashDrawerModelScene, cashDrawerTypeOfApplicationScene, cashDrawerContractFileScene, cashDrawerConfirmationScene, cashDrawerPreHomeEnterScene, cashDrawerPreBackEnterScene, cashDrawerTypeOfApplicationSubScene } = require('./src/controllers/cashDrawer')
const { merchantHomeScene, merchantListShopsScene, merchantAddShopScene, merchantListCashDrawersScene, merchantMenuExitScene, merchantMenuNoConnectionScene, merchantMenuNotEnoughMoneyScene, merchantCashDrawerUploadFileScene, merchantReviceScene } = require('./src/controllers/merchantMenu')
const { faqEnterScene, faqShopScene, faqCashDrawerScene, faqReviceScene, faqGiveQuestionScene, faqPreBackScene } = require('./src/controllers/faq')
require('dotenv').config()

const bot = new Telegraf(process.env.BOT_TOKEN, {}) 
const stage = new Scenes.Stage([
  startScene,
  startEnterTinScene,
  // merchantEnterScene,
  merchantScene,
  merchantNameScene,
  merchantPhoneScene,
  merchantPasswordScene,
  merchantConfirmationScene,
  shopEnterBackScene,
  shopEnterHomeScene,
  shopNameScene,
  shopRegionScene,
  shopDistrictScene,
  shopActivityGroupScene,
  shopActivityTypeScene,
  shopCadastreScene,
  shopAddressScene,
  shopLocationScene,
  shopFileScene,
  shopConfirmationScene,
  cashDrawerPreHomeEnterScene,
  cashDrawerPreBackEnterScene,
  // cashDrawerEnterScene,
  cashDrawerModelScene,
  cashDrawerTypeOfApplicationScene,
  cashDrawerTypeOfApplicationSubScene,
  cashDrawerContractFileScene,
  cashDrawerConfirmationScene,
  merchantHomeScene,
  merchantReviceScene,
  merchantAddShopScene,
  merchantListShopsScene,
  merchantListCashDrawersScene,
  merchantCashDrawerUploadFileScene,
  merchantMenuExitScene,
  merchantMenuNoConnectionScene,
  merchantMenuNotEnoughMoneyScene,
  faqPreBackScene,
  faqEnterScene,
  faqShopScene,
  faqCashDrawerScene,
  faqReviceScene,
  faqGiveQuestionScene
])
bot.use(session())
bot.use(stage.middleware())
bot.start(asyncWrapper(ctx => {
  // ctx.session[ctx.update.message.chat.id] = ctx.update.message.chat.id 
  ctx.scene.enter('start')
}))
bot.command(['/shop'], ctx => {
  ctx.scene.enter(scenes.shop.shopName)
  console.log(ctx.chat.id)
})
bot.command(['/d'], ctx => {
  ctx.scene.enter(scenes.cashDrawer.enter)
  console.log(ctx.chat.id)
})

bot.command(['/p'], ctx => {
  ctx.scene.enter(scenes.merchantPassword)
  console.log(ctx.chat.id)
})
bot.command(['/m'], ctx => {
  ctx.scene.enter(scenes.merchantMenu.home)
  console.log(ctx.chat.id)
})
bot.on('location', ctx => console.log(ctx.update.message.location))
bot.hears('ctx', ctx => console.log(ctx.update.message))

bot.hears('a', ctx => {
  const remoteFile = 'http://library.ziyonet.uz/ru/book/download/55331'
  ctx.replyWithDocument({url: remoteFile})
  ctx.replyWithDocument({url: "http://10.0.2.202:8005/api/sales/support/lite/all-stores/upload/files/1624542591047_dog.pdf"})
  // bot.telegram.sendDocument(ctx.chat.id, remoteFile, [{disable_notification: true}]);
  
})

bot.on('forward_date', ctx => ctx.deleteMessage())

bot.on('document', async ctx => {

  // const {file_id: fileId} = ctx.update.message.document;
  const fileUrl = await ctx.telegram.getFileLink(ctx.update.message.document.file_id);
  // const file = fs.createWriteStream('file.docx');
  // const request = https.get(fileUrl.href, async function(response) {
  //   const res = await response.pipe(file);
    ctx.reply(fileUrl.href)
  // });
  // const res = await axios.get(fileUrl.href)  
  // const url = fileUrl.href; // link to file you want to download
  // const path = __dirname // where to save a file

  // const request = https.get(url, function(response) {
  //     if (response.statusCode === 200) {
  //         var file = fs.createWriteStream(path);
  //         console.log(file)
  //         response.pipe(file);
  //     }
  //     request.setTimeout(60000, function() { // if after 60s file not downlaoded, we abort a request 
  //         request.abort();
  //     });
  // });
  // const fileUrwl = new URL(fileUrl.pathname);
  // console.log(fs.readFileSync(fileUrwl));
  // console.log(fs.readlink(fileUrl.href))
  // const file = fs.createWriteStream("file.docx")
  // console.log(fileUrl.)
//   const response = await axios.get(fileUrl)
  // console.log(fileUrl)
//   ctx.reply('I read the file for you! The contents were:\n\n' + response);
})

// bot.start((ctx) => {
//     bot.telegram.sendMessage(
//         ctx.chat.id, 
//         ` Здравствуйте, уважаемый клиент ${ctx.from.first_name}! Тут Вы можете запросить акт сверки. Введите свой ИНН и получите данные.`, 
//         {
//             reply_markup: {
//                 inline_keyboard: selectPart
//             }
//         }
//     )
// }) 
// bot.action(botCommands.signUpNits, ctx => {
//     ctx.deleteMessage()
//     bot.telegram.sendMessage(
//         ctx.chat.id, 
//         'enter your tin',
//         {
//             reply_markup: {
//                 inline_keyboard: buttonBack
//             }
//         }
//     )
//     bot.on('text', async ctx => {
//         try {
//             const response = await Api({
//                 url: `${apiUrles.checkBalance}?tin=${ctx.message.text}`
//             })
//             console.log(response)
//         } catch (e) {
//             console.log(e)
//         }
//     })
// })
// bot.action(botCommands.accounting, ctx => {
//     console.log(ctx.update)
//     // ctx.deleteMessage()
//     bot.telegram.sendMessage(
//         ctx.chat.id, 
//         'you are in buh',
//         {
//             reply_to_message_id: 0
//             // reply_markup: {
//             //     inline_keyboard: selectPart
//             // }
//         }
//     )
// })

bot.launch() 